// Import the carmanager_api library
extern crate carmanager_api;

#[cfg(test)]
mod test {

    use carmanager_api::car::models::{Car, NewCar};

    #[test]
    fn car_compare_works() {
        let first_car = Car {
            id: 1,
            regnumber: String::from("ABC-123"),
            model: String::from("ModelA")
        };

        let second_car = first_car.clone();
        assert_eq!(first_car, second_car);

        let third_car = Car {
            id: 1,
            regnumber: String::from("DEF-456"),
            model: String::from("ModelA")
        };
        assert_ne!(first_car, third_car);

        let fourth_car = Car {
            id: 1,
            regnumber: String::from("ABC-123"),
            model: String::from("ModelB")
        };
        assert_ne!(first_car, fourth_car);

        let fifth_car = Car {
            id: 1,
            regnumber: String::from("abc-123"),
            model: String::from("modela")
        };
        assert_ne!(first_car, fifth_car);

        let sixth_car = Car {
            id: 999,
            regnumber: String::from("ABC-123"),
            model: String::from("ModelA")
        };
        assert_ne!(first_car, sixth_car);
    }

    #[test]
    fn new_car_compare_works() {
        let first_car = NewCar {
            regnumber: String::from("ABC-123"),
            model: String::from("ModelA")
        };

        let second_car = first_car.clone();
        assert_eq!(first_car, second_car);

        let third_car = NewCar {
            regnumber: String::from("DEF-456"),
            model: String::from("ModelA")
        };
        assert_ne!(first_car, third_car);

        let fourth_car = NewCar {
            regnumber: String::from("ABC-123"),
            model: String::from("ModelB")
        };
        assert_ne!(first_car, fourth_car);

        let fifth_car = NewCar {
            regnumber: String::from("abc-123"),
            model: String::from("modela")
        };
        assert_ne!(first_car, fifth_car);
    }

}