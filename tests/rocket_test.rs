// Import the carmanager_api library
extern crate carmanager_api;
// Import other dependencies
extern crate rocket;
extern crate rocket_contrib;
extern crate serde_json;

#[cfg(test)]
mod test {
    use rocket::local::Client;
    use rocket::http::{ContentType, Status};
    use serde_json::Value;

    use std::time::SystemTime;

    use carmanager_api::car::models::{Car, NewCar};

    fn setup_rocket() -> rocket::Rocket {
        use std::env;
        // Assert that the DATABASE_URL variable exists
        let test_database_url = env::var("TEST_DATABASE_URL").expect("TEST_DATABASE_URL was not set!");
        carmanager_api::rocket(test_database_url)
    }

    /**
     * Print the amount of milliseconds since the given timestamp.
     */
    fn get_str_millis_since(start_time: &SystemTime) -> String {
        match SystemTime::now().duration_since(*start_time) {
            Ok(elapsed) => format!("{} ms", elapsed.as_millis()),
            Err(e) => format!("Could not get test duration. Error: {:?}", e),
        }
    }

    fn print_subtest_started(message: &String) {
        println!("{} - Started...", message);
    }

    fn print_subtest_done(message: &String, subtest_start_time: &SystemTime) {
        println!("{} - OK (took {})", message, get_str_millis_since(&subtest_start_time));
    }

    /**
     * Compare two vectors of Car structs.
     * If the vectors contain equal cars, or both are empty, return true.
     * Otherwise, return false.
     */
    fn compare_car_vecs(one: &Vec<Car>, other: &Vec<Car>, is_sorted: bool) -> bool {
        // Check the sizes
        if one.len() != other.len() {
            return false;
        } else if one.len() == 0 {
            return true;
        }

        // Check the contents by looping.
        if is_sorted {
            // We can assume that the cars are sorted in the same fashion
            for i in 0..one.len() {
                if one[i] != other[i] {
                    return false;
                }
            }
        } else {
            // We can't assume the ordering... Need to do a double loop.
            // We should probably clone the other vector, and remove items
            // which have been matched already.
            let mut other_cloned = other.clone();
            for car in one.iter() {
                let mut found = false;
                let mut found_index: usize = 0;
                'inner: for i in 0..other_cloned.len() {
                    let other_car: &Car = &other_cloned[i];
                    if car == other_car {
                        found = true;
                        found_index = i;
                        // Stop the inner loop
                        break 'inner;
                    }
                }
                // If we exited the inner loop and found is still false, the comparison failed.
                if !found {
                    return false;
                } else {
                    // We found a match. Remove the element before proceeding with the next search.
                    other_cloned.remove(found_index);
                }
            }
        }

        // If nothing went wrong so far, return true.
        return true;
    }

    fn test_get_all_cars(client: &Client, expected_cars: &Vec<Car>) {
        let mut response = client.get("/api/v1/cars").dispatch();
        assert_eq!(response.status(), Status::Ok);
        let body_string = response.body_string().unwrap();
        // Deserialize the JSON array that is received.
        let received_cars: Vec<Car> = serde_json::from_str(&body_string).unwrap();
        let result_is_expected = compare_car_vecs(&received_cars, &expected_cars, false);
        assert_eq!(result_is_expected, true);
    }

    fn test_get_car_by_id_expecting_failure(client: &Client, id: i32) {
        let response = client.get(format!("/api/v1/cars/{}", id)).dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    fn test_get_car_by_id_expecting_success(client: &Client, id: i32, expected_car: &Car) {
        let mut response = client.get(format!("/api/v1/cars/{}", id)).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let body_string = response.body_string().unwrap();
        // Deserialize the Car object
        let car: Car = serde_json::from_str(&body_string).unwrap();
        assert_eq!(car.regnumber, expected_car.regnumber);
        assert_eq!(car.model, expected_car.model);
        assert_eq!(car.id, id);
    }

    fn test_insert_new_car(client: &Client, new_car: NewCar, expected_id: i32) -> Car {
        let json_data = serde_json::to_string(&new_car).unwrap();
        let post_request = client.post("/api/v1/cars")
            .header(ContentType::JSON)
            .body(&json_data);
        let mut response = post_request.dispatch();
        assert_eq!(response.status(), Status::Ok);
        let body_string = response.body_string().unwrap();
        // Deserialize the Car object
        let car: Car = serde_json::from_str(&body_string).unwrap();
        // Check that the returned data is what we expected.
        assert_eq!(car.regnumber, new_car.regnumber);
        assert_eq!(car.model, new_car.model);
        assert_eq!(car.id, expected_id);

        // Return the Car which was created, so that we can keep track of them.
        return car;
    }

    fn get_cars_by_model_endpoint(is_exact_model: bool) -> String {
        if is_exact_model {
            // Exactly match agains the model name
            return String::from("/api/v1/cars/models");
        } else {
            // Search by model name
            return String::from("/api/v1/cars/search/model");
        }
    }

    fn test_get_cars_by_model(client: &Client, model: &String, exact_match: bool, expected_cars: &Vec<Car>) {
        let endpoint = get_cars_by_model_endpoint(exact_match);
        let mut response = client.get(format!("{}/{}", endpoint, model)).dispatch();
        // If we don't expect any results, the API should return 404.
        if expected_cars.is_empty() {
            assert_eq!(response.status(), Status::NotFound);
        } else {
            assert_eq!(response.status(), Status::Ok);
            let body_string = response.body_string().unwrap();
            // Deserialize the Car object
            let received_cars: Vec<Car> = serde_json::from_str(&body_string).unwrap();
            let result_is_expected = compare_car_vecs(&received_cars, &expected_cars, false);
            assert_eq!(result_is_expected, true);
        }
    }

    fn test_get_cars_by_exact_model(client: &Client, model: &String, expected_cars: &Vec<Car>) {
        test_get_cars_by_model(&client, &model, true, &expected_cars);
    }

    fn test_search_cars_by_model(client: &Client, model: &String, expected_cars: &Vec<Car>) {
        test_get_cars_by_model(&client, &model, false, &expected_cars);
    }

    fn test_search_cars_by_regnumber(client: &Client, regnumber: &String, expected_cars: &Vec<Car>) {
        let endpoint = "/api/v1/cars/search/regnumber";
        let mut response = client.get(format!("{}/{}", endpoint, regnumber)).dispatch();
        // If we don't expect any results, the API should return 404.
        if expected_cars.is_empty() {
            assert_eq!(response.status(), Status::NotFound);
        } else {
            assert_eq!(response.status(), Status::Ok);
            let body_string = response.body_string().unwrap();
            // Deserialize the Car object
            let received_cars: Vec<Car> = serde_json::from_str(&body_string).unwrap();
            let result_is_expected = compare_car_vecs(&received_cars, &expected_cars, false);
            assert_eq!(result_is_expected, true);
        }
    }

    fn test_updating_car_by_id(client: &Client, id: i32, updated_car: NewCar, expecting_success: bool) -> Option<Car> {
        let endpoint = format!("/api/v1/cars/{}", id);
        let json_data = serde_json::to_string(&updated_car).unwrap();
        let post_request = client.put(endpoint)
            .header(ContentType::JSON)
            .body(&json_data);
        let mut response = post_request.dispatch();
        if !expecting_success {
            assert_eq!(response.status(), Status::NotFound);
            return None;
        } else {
            assert_eq!(response.status(), Status::Ok);
            let body_string = response.body_string().unwrap();
            // Deserialize the Car object
            let returned_car: Car = serde_json::from_str(&body_string).unwrap();
            // Check that the returned data is what we expected.
            assert_eq!(returned_car.regnumber, updated_car.regnumber);
            assert_eq!(returned_car.model, updated_car.model);
            assert_eq!(returned_car.id, id);

            // Return the Car which was updated, so that we can keep track of it.
            return Some(returned_car);
        }
    }

    fn test_delete_car_by_id(client: &Client, id: i32, expecting_success: bool) {
        let endpoint = format!("/api/v1/cars/{}", id);
        let delete_request = client.delete(endpoint);
        let mut response = delete_request.dispatch();
        if !expecting_success {
            assert_eq!(response.status(), Status::NotFound);
        } else {
            assert_eq!(response.status(), Status::Ok);
            let body_string = response.body_string().unwrap();
            println!("Delete test returned body string: {}", body_string);
            let returned_json: Value = serde_json::from_str(&body_string).unwrap();
            assert_eq!(returned_json["deleted"], id);
        }
    }

    /**
     * Test that the rocket routes and database integrations are working as intended.
     * The tests need to be executed in a single test case, in order to guarantee that
     * the database state is predictable. The test assumes that the database is empty
     * when the test starts.
     */
    #[test]
    fn rocket_tests() {
        let test_start_time = SystemTime::now();
        // Launch rocket + get a client
        let rocket = setup_rocket();
        let client = Client::new(rocket).unwrap();

        let mut expected_cars_in_db: Vec<Car> = Vec::new();
        let mut next_car_id = 1;

        // Get all test
        let message = String::from("Test getting all cars when the DB is empty");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_get_all_cars(&client, &expected_cars_in_db);
        print_subtest_done(&message, &subtest_start_time);

        // Get by ID that isn't found
        let message = String::from("Test getting a car by ID from an empty DB (should fail with 404)");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_get_car_by_id_expecting_failure(&client, 1);
        print_subtest_done(&message, &subtest_start_time);

        // Test inserting a car
        let first_new_car: NewCar = NewCar {
            regnumber: String::from("ABC-123"),
            model: String::from("TestModel")
        };
        let message = String::from("Test inserting a car");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        let first_car: Car = test_insert_new_car(&client, first_new_car, next_car_id.clone());
        next_car_id += 1;
        // Unless an assert has failed, we should have a valid Car.
        // Add it to the expected_cars_in_db list.
        expected_cars_in_db.push(first_car);
        print_subtest_done(&message, &subtest_start_time);

        // Get by ID that is found
        let message = String::from("Test getting an existing car by ID");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_get_car_by_id_expecting_success(&client, 1, &expected_cars_in_db[0]);
        print_subtest_done(&message, &subtest_start_time);

        // Get all test, again
        let message = String::from("Test getting all cars when there is one car in the DB");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_get_all_cars(&client, &expected_cars_in_db);
        print_subtest_done(&message, &subtest_start_time);

        // Insert a few cars with the same model name
        let model_test_insert_count = 10;
        let message = format!("Test inserting {} cars", model_test_insert_count);
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        let test_model_name = String::from("TestExampleModel");
        let mut expected_cars_with_same_model_name: Vec<Car> = Vec::new();
        for i in 1..=model_test_insert_count {
            // Create a NewCar object, with the same model name, but different regnumbers
            let generated_regnumber = format!("TEST-{}", i);
            let new_car = NewCar {
                regnumber: generated_regnumber,
                model: test_model_name.clone()
            };
            let next_expected_id = next_car_id;
            // Do the insert
            let returned_car: Car = test_insert_new_car(&client, new_car, next_expected_id);
            next_car_id += 1;
            expected_cars_in_db.push(returned_car.clone());
            expected_cars_with_same_model_name.push(returned_car);
        }
        print_subtest_done(&message, &subtest_start_time);

        // Get all test, again
        let message = String::from("Test getting all cars when there is more cars in the DB");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_get_all_cars(&client, &expected_cars_in_db);
        print_subtest_done(&message, &subtest_start_time);

        // Test getting all cars by model name (exact match)
        let message = format!("Test getting all cars with model name '{}'", &test_model_name);
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_get_cars_by_exact_model(&client, &test_model_name, &expected_cars_with_same_model_name);
        print_subtest_done(&message, &subtest_start_time);

        // Test getting all cars by model name (exact match), for a model that doesn't exist
        let test_model_name_nonexistent = String::from("ThisModelNameShouldNotExistBecauseItsSoLong");
        let message = format!("Test getting all cars with model name '{}' and expect no results", &test_model_name_nonexistent);
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_get_cars_by_exact_model(&client, &test_model_name_nonexistent, &Vec::new());
        print_subtest_done(&message, &subtest_start_time);

        // Test searching for cars by model name
        // "Test" should match all cars so far
        let model_search_term = String::from("Test");
        let message = format!("Test searching for cars whose model name match '{}'", &model_search_term);
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_search_cars_by_model(&client, &model_search_term, &expected_cars_in_db);
        print_subtest_done(&message, &subtest_start_time);

        // Test searching for cars with a model name that doesn't exist
        let message = format!("Test searching for cars whose model name match '{}' and expect no results", &test_model_name_nonexistent);
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_search_cars_by_model(&client, &test_model_name_nonexistent, &Vec::new());
        print_subtest_done(&message, &subtest_start_time);

        // Test searching for cars by regnumber
        // "TEST" should match the inserted cars from the loop above.
        let regnumber_search_term = String::from("TEST");
        let message = format!("Test searching for cars whose regnumbers match '{}'", &regnumber_search_term);
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_search_cars_by_regnumber(&client, &regnumber_search_term, &expected_cars_with_same_model_name);
        print_subtest_done(&message, &subtest_start_time);

        // Test searching for cars by a regnumber that doesn't exist in the DB.
        let regnumber_search_term = String::from("This_regnumber_doesnt_exist_in_the_DB");
        let message = format!("Test searching with a regnumber that doesn't exist in the DB: '{}'", &regnumber_search_term);
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        // Expecting no results from the search --> use an empty vector.
        test_search_cars_by_regnumber(&client, &regnumber_search_term, &Vec::new());
        print_subtest_done(&message, &subtest_start_time);

        // Test updating (i.e. PUT:ting) cars
        // First, create a new car to start with.
        let new_car_to_update = NewCar {
            regnumber: String::from("XYZ-000"),
            model: String::from("ModelToUpdate")
        };

        let message = String::from("Test inserting a car which we'll update later");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        let car_to_update: Car = test_insert_new_car(&client, new_car_to_update, next_car_id.clone());
        let id_of_car_to_update = car_to_update.id;
        next_car_id += 1;
        // Add it to the expected_cars_in_db list.
        expected_cars_in_db.push(car_to_update);
        print_subtest_done(&message, &subtest_start_time);

        let message = String::from("Test updating the car with a new model and regnumber");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        let new_car_with_updates = NewCar {
            regnumber: String::from("XYZ-999"),
            model: String::from("ModelToUpdate123")
        };
        let updated_car = test_updating_car_by_id(&client, id_of_car_to_update, new_car_with_updates.clone(), true).unwrap();
        // Replace the previous Car in the expected vector
        expected_cars_in_db.pop();
        expected_cars_in_db.push(updated_car);
        print_subtest_done(&message, &subtest_start_time);

        let message = String::from("Test updating a car ID that doesn't exist");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        let id_which_does_not_exist = 10000;
        test_updating_car_by_id(&client, id_which_does_not_exist, new_car_with_updates, false);
        print_subtest_done(&message, &subtest_start_time);

        // Get all test, again
        let message = String::from("Test getting all cars when there is more cars and an updated car in the DB");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_get_all_cars(&client, &expected_cars_in_db);
        print_subtest_done(&message, &subtest_start_time);

        // Test deleting a car
        let message = String::from("Test deleting the updated car");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        test_delete_car_by_id(&client, id_of_car_to_update, true);
        // Remove the deleted car from the expected vector
        expected_cars_in_db.pop();
        print_subtest_done(&message, &subtest_start_time);

        // Test deleting a car that doesn't exist
        let message = String::from("Test deleting a non-existent car");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        let id_that_doesnt_exist = 10000;
        test_delete_car_by_id(&client, id_that_doesnt_exist, false);
        print_subtest_done(&message, &subtest_start_time);

        // Test that inserting a new car uses the expected next id
        let new_car_after_delete = NewCar {
            regnumber: String::from("CBA-321"),
            model: String::from("ModelAfterDeleteTest")
        };
        let message = String::from("Test inserting a car and check that its ID is correctly assigned after the deletion");
        print_subtest_started(&message);
        let subtest_start_time = SystemTime::now();
        let car_inserted_after_delete: Car = test_insert_new_car(&client, new_car_after_delete, next_car_id.clone());
        next_car_id += 1;
        // Add it to the expected_cars_in_db list.
        expected_cars_in_db.push(car_inserted_after_delete);
        print_subtest_done(&message, &subtest_start_time);

        println!("All tests took: {}", get_str_millis_since(&test_start_time));
    }
}
