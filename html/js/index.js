var app = new Vue({
	el: '#getAllCarsApp',
	data: {
		header_text: 'Get all cars',
		message: 'Get all cars from the database by pushing the button.',
		button_text: 'Get cars'
	},
	methods: {
		getAllCars: function () {
			let xhttp = new XMLHttpRequest();
			xhttp.onload = function() {
				// Get the resultsText paragraph
				let resultsText = document.getElementById("resultsText");
				let resultsListDiv = document.getElementById("resultsList");
				if (this.status == 200) {
					// Put the resulting text into the paragraph
					resultsText.innerHTML = xhttp.responseText;
					putCarListIntoDiv(resultsListDiv, xhttp.response);
				} else {
					// Put an error text into it if the request failed
					console.log("Error: " + xhttp.response);
					resultsText.innerHTML = "Error: " + xhttp.responseText;
				}
			};
			xhttp.open("GET", "/api/v1/cars", true);
			xhttp.send();
		}
	}
});

function putCarListIntoDiv(divElement, jsonText) {
	json = JSON.parse(jsonText);
	let resultArray = json.result;
	if (resultArray.length > 0) {
		let orderedList = document.createElement("ol");
		resultArray.forEach(element => {
			let listItem = document.createElement("li");
			listItem.innerHTML = element.regnumber + " - " + element.model;
			orderedList.appendChild(listItem);
		});
		// Remove the previously generated list
		if (divElement.childNodes.length > 0) {
			divElement.removeChild(divElement.childNodes[0]);
		}
		// Put in the new list
		divElement.appendChild(orderedList);
	} else {
		console.log("The result list is empty.");
	}
};


