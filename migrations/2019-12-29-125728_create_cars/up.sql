-- Your SQL goes here

CREATE TABLE cars (
    id SERIAL PRIMARY KEY,
    regnumber VARCHAR UNIQUE NOT NULL,
    model VARCHAR NOT NULL
)
