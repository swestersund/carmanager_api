FROM rustlang/rust:nightly-buster-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends libpq-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* ;

USER 1000:1000

VOLUME [ "/carmanager_api" ]

WORKDIR /carmanager_api

CMD [ "tail" "-f" "/dev/null" ]
