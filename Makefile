# Makefile for carmanager_api
# This makefile is useful for building either the debug or release configurations
# for carmanager_api, either just the binary, or also the container image.

SHELL = /bin/bash

# File variables
ENV_FILE = .env
ENV_SUGGESTION_FILE = env.suggestion
SOURCE_FILES = $(shell test -e src/ && find src -type f)
TEST_FILES = $(shell test -e tests/ && find src -type f)
CARGO_FILES = Cargo.toml Cargo.lock
MIGRATION_FILES = $(shell test -e migrations/ && find migrations -type f)
HTML_FILES = $(shell test -e html/ && find html -type f)
BUILDER_DOCKERFILE = builder.dockerfile
CONTAINER_FILES = Dockerfile $(BUILDER_DOCKERFILE)
DOCKER_FILES = docker-compose.yml $(CONTAINER_FILES)
PODMAN_FILES = podman-compose.yml $(CONTAINER_FILES)
# Executable names
CARMANAGER_API_DEBUG = target/debug/carmanager_api
CARMANAGER_API_RELEASE = target/release/carmanager_api

# Test variables
POSTGRES_USER = postgres
POSTGRES_PASSWORD = m6DxpNPW2dqFgB7SgmGv
POSTGRES_DB = carmanager_api_test
POSTGRES_PORT = 5432
TEST_COMPOSE_NAME = test-docker-compose.yml
TEST_ENV_FILE = .test_env_$(shell date +%s)

# Build settings
CARGO_TOOLCHAIN = +nightly
# Automatically check the ".env" file which "CONTAINER_TOOL" is chosen.
CONTAINER_TOOL = $(shell test -f .env && grep "CONTAINER_TOOL" .env | cut -d= -f 2 || echo "uninitialized_container_tool")


debug: $(ENV_FILE) $(CARMANAGER_API_DEBUG) container-debug
release: $(ENV_FILE) $(CARMANAGER_API_RELEASE) container-release
test: cargo-test

# ---------------------
# Copy the env.example file to .env if the user hasn't already done that
$(ENV_SUGGESTION_FILE): env.example
	@echo "Creating $(ENV_SUGGESTION_FILE) file..."
	cp env.example $(ENV_SUGGESTION_FILE)
	@echo "Checking if you have docker or podman..."
	if which docker >/dev/null; then sed -i 's|^CONTAINER_TOOL=.*$$|CONTAINER_TOOL=docker|' $(ENV_SUGGESTION_FILE) ; fi
	if which podman >/dev/null; then sed -i 's|^CONTAINER_TOOL=.*$$|CONTAINER_TOOL=podman|' $(ENV_SUGGESTION_FILE) ; fi
	@echo -n "Your suggested container tool: "
	@grep 'CONTAINER_TOOL' $(ENV_SUGGESTION_FILE)
	@echo "Generating a random password for your PostgreSQL data... It will be written into the $(ENV_SUGGESTION_FILE) file."
	@sed -i "s|^POSTGRES_PW=.*$$|POSTGRES_PW=$(shell tr -dc A-Za-z0-9 </dev/urandom | head -c 32)|" $(ENV_SUGGESTION_FILE)

$(ENV_FILE): env.example $(ENV_SUGGESTION_FILE)
	@echo "Copying the $(ENV_SUGGESTION_FILE) file to '.env' (if it doesn't exist). This is needed to run carmanager_api."
	# Don't overwrite $(ENV_FILE) if it already exists
	cp -n $(ENV_SUGGESTION_FILE) $(ENV_FILE)


# ---------------------
# Build the carmanager_api_builder container
container-builder: $(ENV_FILE) $(BUILDER_DOCKERFILE)
	sudo $(CONTAINER_TOOL) build -t carmanager_api_builder:latest -f $(BUILDER_DOCKERFILE) .

docker-builder: $(BUILDER_DOCKERFILE)
	sudo docker build -t carmanager_api_builder:latest -f $(BUILDER_DOCKERFILE) .

podman-builder: $(BUILDER_DOCKERFILE)
	sudo podman build -t carmanager_api_builder:latest -f $(BUILDER_DOCKERFILE) .

# ---------------------
# Build the executables

$(CARMANAGER_API_DEBUG): $(SOURCE_FILES) $(CARGO_FILES)
	cargo $(CARGO_TOOLCHAIN) build

$(CARMANAGER_API_RELEASE): $(SOURCE_FILES) $(CARGO_FILES)
	cargo $(CARGO_TOOLCHAIN) build --release

# The Debug executable (local, non-containerized build)
cargo-debug: $(SOURCE_FILES) $(CARGO_FILES)
	cargo $(CARGO_TOOLCHAIN) build

# The Debug executable (containerized build)
container-build-debug: $(SOURCE_FILES) $(CARGO_FILES) container-builder
	sudo $(CONTAINER_TOOL) run -it -v $(shell pwd):/carmanager_api carmanager_api_builder:latest cargo $(CARGO_TOOLCHAIN) build


# The Release executable
cargo-release: $(SOURCE_FILES) $(CARGO_FILES)
	cargo $(CARGO_TOOLCHAIN) build --release

container-build-release: $(SOURCE_FILES) $(CARGO_FILES) container-builder
	sudo $(CONTAINER_TOOL) run -it -v $(shell pwd):/carmanager_api carmanager_api_builder:latest cargo $(CARGO_TOOLCHAIN) build --release

# ---------------------
# Build the carmanager_api container (depends on the executable + files which are added into the container image)

container-debug: $(CARMANAGER_API_DEBUG) $(HTML_FILES) $(CONTAINER_FILES) $(MIGRATION_FILES)
	sudo $(CONTAINER_TOOL) build -t carmanager_api:latest --build-arg buildtype=debug .

container-release: $(CARMANAGER_API_RELEASE) $(HTML_FILES) $(CONTAINER_FILES) $(MIGRATION_FILES)
	sudo $(CONTAINER_TOOL) build -t carmanager_api:latest --build-arg buildtype=release .

# Explicit Docker targets
docker-debug: $(CARMANAGER_API_DEBUG) $(HTML_FILES) $(DOCKER_FILES) $(MIGRATION_FILES)
	sudo docker build -t carmanager_api:latest --build-arg buildtype=debug .

docker-release: $(CARMANAGER_API_RELEASE) $(HTML_FILES) $(DOCKER_FILES) $(MIGRATION_FILES)
	sudo docker build -t carmanager_api:latest --build-arg buildtype=release .

# Explicit Podman targets
podman-debug: $(CARMANAGER_API_DEBUG) $(HTML_FILES) $(PODMAN_FILES) $(MIGRATION_FILES)
	sudo podman build -t carmanager_api:latest --build-arg buildtype=debug .

podman-release: $(CARMANAGER_API_RELEASE) $(HTML_FILES) $(PODMAN_FILES) $(MIGRATION_FILES)
	sudo podman build -t carmanager_api:latest --build-arg buildtype=release .


# ---------------------
# Run the containers with compose
run-podman: podman-debug $(PODMAN_FILES) $(ENV_FILE)
	sudo podman-compose -f podman-compose.yml up -d

run-podman-release: podman-release $(PODMAN_FILES) $(ENV_FILE)
	sudo podman-compose -f podman-compose.yml up -d

run-docker: docker-debug $(DOCKER_FILES) $(ENV_FILE)
	sudo docker-compose -f docker-compose.yml up -d

run-docker-release: docker-release $(DOCKER_FILES) $(ENV_FILE)
	sudo docker-compose -f docker-compose.yml up -d

# ---------------------
# Run tests
# - Start a postgres test container
# - Run the cargo test command
# - Stop the postgres test container

# Work with the local cargo installation
cargo-test: $(TEST_FILES) cargo-debug
	# Write the environment variables for docker-compose into a temporary file
	@if [ "$(CONTAINER_TOOL)" = "podman" ]; then echo "Sorry, podman is not supported yet..."; fi
	@ > $(TEST_ENV_FILE)
	@echo "TEST_POSTGRES_USER=$(POSTGRES_USER)" >> $(TEST_ENV_FILE)
	@echo "TEST_POSTGRES_PW=$(POSTGRES_PASSWORD)" >> $(TEST_ENV_FILE)
	@echo "TEST_POSTGRES_DB=$(POSTGRES_DB)" >> $(TEST_ENV_FILE)
	@echo "TEST_POSTGRES_PORT=$(POSTGRES_PORT)" >> $(TEST_ENV_FILE)
	# Start the test database
	@echo "Starting a test container with Postgres"
	sudo docker-compose \
		--env-file $(TEST_ENV_FILE) \
		-f $(TEST_COMPOSE_NAME) \
		up -d
	# Wait a bit for the container to initialize
	sleep 3
	# Run the tests
	@echo "Running cargo tests"
	TEST_DATABASE_URL=postgres://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@127.0.0.1:$(POSTGRES_PORT)/$(POSTGRES_DB) \
		cargo $(CARGO_TOOLCHAIN) test -- --nocapture
	# Tear down the test container
	@echo "Stopping the test container with Postgres"
	@sudo docker-compose \
		--env-file $(TEST_ENV_FILE) \
		-f $(TEST_COMPOSE_NAME) \
		down
	# Remove the temporary file
	@rm -f $(TEST_ENV_FILE)

# ---------------------
# Calculate test coverage
coverage: clean
	# Set some environment variables for tracking coverage when building and testing
	CARGO_INCREMENTAL=0 \
		RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests -Cpanic=abort" \
		RUSTDOCFLAGS="-Cpanic=abort" \
		$(MAKE) cargo-debug

	CARGO_INCREMENTAL=0 \
		RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests -Cpanic=abort" \
		RUSTDOCFLAGS="-Cpanic=abort" \
		$(MAKE) cargo-test

	grcov ./target/debug/ -t html --llvm --branch --ignore-not-existing -o ./target/debug/coverage/
	@echo "See the coverage report at: ./target/debug/coverage/index.html"

# ---------------------
clean:
	cargo $(CARGO_TOOLCHAIN) clean
