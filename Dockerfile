FROM debian:buster-slim

LABEL author="Simon"
ARG buildtype=debug
ARG rocket_env=staging

ENV ROCKET_ENV=${rocket_env}

RUN apt-get update \
    && apt-get install --no-install-recommends -y libpq5 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* ;

COPY target/${buildtype}/carmanager_api /carmanager_api
COPY ./migrations /migrations

RUN ["chmod", "-R", "0700", "/carmanager_api", "/migrations"]

VOLUME [ "/html" ]
EXPOSE 8000/tcp
CMD ["/carmanager_api"]
