#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate diesel;

extern crate r2d2;
extern crate r2d2_diesel;
#[macro_use] extern crate rocket;
extern crate rocket_contrib;
#[macro_use] extern crate serde_json;

use rocket_contrib::serve::StaticFiles;

mod db;
pub mod car;

pub fn rocket(database_url: String) -> rocket::Rocket {
    println!("Using database url: {}", database_url);
    let pool = db::init_pool(database_url);
    // Open a new scope to let the migration_connection be freed as soon as possible
    {
        // Take a connection from the pool to run the embedded database migrations
        let migration_connection = pool.get().expect("Could not get a database connection.");
        diesel_migrations::run_pending_migrations(&*migration_connection).expect("A migration failed.");
    }

    use car::routes::*;
    rocket::ignite()
        .manage(pool)
        .mount(
            "/api/v1/cars",
            routes![get_all_cars, insert_new_car, insert_new_car_from_form, show_car_with_id,
                    delete_car_with_id, get_cars_by_model, update_car_with_id, search_cars_by_regnumber,
                    search_cars_by_model],
        )
        .mount("/public", StaticFiles::from("html"))
}