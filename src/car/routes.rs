use rocket::request::Form;
use rocket_contrib::json::Json;
use serde_json::Value;
use super::super::db::Conn as DbConn;
use super::models::{Car, NewCar};

#[get("/")]
pub fn get_all_cars(conn: DbConn) -> Json<Value> {
    let cars = Car::all(&conn);

    Json(json!(cars))
}

#[post("/", format = "application/json", data = "<new_car>")]
pub fn insert_new_car(conn: DbConn, new_car: Json<NewCar>) -> Option<Json<Value>> {
    let success = Car::insert(new_car.into_inner(), &conn);
    if success {
        return Some(Json(json!(Car::all(&conn).first())));
    } else {
        return None;
    }
}

// Function for POSTing a new car via a HTML Form.
#[post("/", format = "application/x-www-form-urlencoded", data = "<form>")]
pub fn insert_new_car_from_form(conn: DbConn, form: Form<NewCar>) -> Option<Json<Value>> {
    insert_new_car(conn, Json(form.into_inner()))
}

#[get("/<id>")]
pub fn show_car_with_id(conn: DbConn, id: i32) -> Option<Json<Value>> {
    let result = Car::show(id, &conn);
    if result.is_empty() {
        return None;
    } else {
        return Some(Json(json!(result.get(0))))
    }
}

#[put("/<id>", format = "application/json", data = "<car>")]
pub fn update_car_with_id(conn: DbConn, id: i32, car: Json<NewCar>) -> Option<Json<Value>> {
    let success = Car::update_by_id(id, &conn, car.into_inner());
    if success {
        return show_car_with_id(conn, id)
    } else {
        println!("Failed to update Car with id = {}", id);
        return None;
    }
}

#[delete("/<id>")]
pub fn delete_car_with_id(id: i32, conn: DbConn) -> Option<Json<Value>> {
    let success = Car::delete_by_id(id, &conn);
    if success {
        return Some(Json(json!({"deleted": id})))
    } else {
        return None;
    }
}

#[get("/models/<model>")]
pub fn get_cars_by_model(model: String, conn: DbConn) -> Option<Json<Value>> {
    let result_list = Car::all_by_model(model, &conn);
    if result_list.is_empty() {
        return None;
    } else {
        return Some(Json(json!(result_list)))
    }
}

#[get("/search/regnumber/<searchterm>")]
pub fn search_cars_by_regnumber(searchterm: String, conn: DbConn) -> Option<Json<Value>> {
    let result_list = Car::search_by_regnumber(searchterm, &conn);
    if result_list.is_empty() {
        return None;
    } else {
        return Some(Json(json!(result_list)))
    }
}

#[get("/search/model/<searchterm>")]
pub fn search_cars_by_model(searchterm: String, conn: DbConn) -> Option<Json<Value>> {
    let result_list = Car::search_by_model(searchterm, &conn);
    if result_list.is_empty() {
        return None;
    } else {
        return Some(Json(json!(result_list)))
    }
}
