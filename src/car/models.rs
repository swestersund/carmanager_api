use diesel;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use serde::{Serialize, Deserialize};
use super::schema::cars;
use super::schema::cars::dsl::cars as all_cars;

#[derive(Serialize, Deserialize, Queryable, Debug, Clone)]
pub struct Car {
    pub id: i32,
    pub regnumber: String,
    pub model: String,
}

#[derive(Serialize, Deserialize, Insertable, FromForm, Debug, Clone)]
#[table_name = "cars"]
pub struct NewCar {
    pub regnumber: String,
    pub model: String,
}

// PartialEq is the same as Eq
impl PartialEq for Car {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id &&
        self.regnumber == other.regnumber &&
        self.model == other.model
    }
}
impl Eq for Car {}

// PartialEq is the same as Eq
impl PartialEq for NewCar {
    fn eq(&self, other: &Self) -> bool {
        self.regnumber == other.regnumber &&
        self.model == other.model
    }
}
impl Eq for NewCar {}

impl Car {
    pub fn show(id: i32, conn: &PgConnection) -> Vec<Car> {
        all_cars
            .find(id)
            .load::<Car>(conn)
            .expect("Error loading car")
    }

    pub fn all(conn: &PgConnection) -> Vec<Car> {
        all_cars
            .order(cars::id.desc())
            .load::<Car>(conn)
            .expect("Error loading all cars")
    }

    pub fn update_by_id(id: i32, conn: &PgConnection, car: NewCar) -> bool {
        use super::schema::cars::dsl::{regnumber as r, model as m};
        let NewCar {
            regnumber,
            model
        } = car;

        diesel::update(all_cars.find(id))
            .set((r.eq(regnumber), m.eq(model)))
            .get_result::<Car>(conn)
            .is_ok()
    }

    pub fn insert(car: NewCar, conn: &PgConnection) -> bool {
        diesel::insert_into(cars::table)
            .values(&car)
            .execute(conn)
            .is_ok()
    }

    pub fn delete_by_id(id: i32, conn: &PgConnection) -> bool {
        if Car::show(id, conn).is_empty() {
            return false;
        };
        diesel::delete(all_cars.find(id)).execute(conn).is_ok()
    }

    pub fn all_by_model(model: String, conn: &PgConnection) -> Vec<Car> {
        all_cars
            .filter(cars::model.eq(model))
            .load::<Car>(conn)
            .expect("Error loading cars by model")
    }

    pub fn search_by_regnumber(search_term: String, conn: &PgConnection) -> Vec<Car> {
        // Rocket guarantees that the search_term is not empty.
        let search_like: String = format!("%{}%", search_term);
        println!("Searching with term: {}", &search_like);
        all_cars
            .filter(cars::regnumber.like(search_like))
            .load::<Car>(conn)
            .expect("Error searching for Cars by regnumber")
    }

    pub fn search_by_model(search_term: String, conn: &PgConnection) -> Vec<Car> {
        // Rocket guarantees that the search_term is not empty.
        let search_like: String = format!("%{}%", search_term);
        println!("Searching with term: {}", &search_like);
        all_cars
            .filter(cars::model.like(search_like))
            .load::<Car>(conn)
            .expect("Error searching for Cars by model")
    }
}
