table! {
    cars (id) {
        id -> Int4,
        regnumber -> Varchar,
        model -> Varchar,
    }
}
