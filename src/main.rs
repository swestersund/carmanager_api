// The following line is needed for Rocket code generation
#![feature(proc_macro_hygiene, decl_macro)]

// Import the carmanager_api library
extern crate carmanager_api;

#[macro_use] extern crate diesel_migrations;

use dotenv::dotenv;
use std::env;

embed_migrations!("./migrations");


fn main() {
    // Read the ".env" file, if it exists
    dotenv().ok();
    // Assert that the DATABASE_URL variable exists
    let database_url = env::var("DATABASE_URL").expect("set DATABASE_URL");

    // Ignite Rocket in the carmanager_api library, then launch it here.
    carmanager_api::rocket(database_url).launch();
}
