# Carmanager API

Carmanager API is an application server for managing cars and vehicles.
The project has served as an exercise for the author to build up a CRUD API
server with Rust and Rocket, and to create DevOps infrastructure and automated
tests around the code.

The project is not of much use currently, as the database models are very simple.
A small, still incomplete HTML front-end is included in this repository.

## Quickstart

Follow these steps for a minimal effort test, which only requires Docker and
Docker-compose.

1. Clone this repository
2. Install docker and docker-compose
3. Build the carmanager_api executable inside a Docker container
4. Build the docker image for carmanager_api
5. Run the application with docker-compose
6. Go to http://localhost:8000/public/index.html and try it out

Here is how to do these steps on Ubuntu 20.04:

```bash
# Step 1
url="the url of this repository"
git clone $url

# Step 2
sudo apt install -y docker docker-compose

# Step 3
make container-build-release
# Now you should have the executable in the ./target/release/ directory

# Step 4
make docker-release

# Step 5
sudo docker-compose -f docker-compose.yml up -d
# Shut down with this command:
# sudo docker-compose -f docker-compose.yml down

# Step 6
curl http://localhost:8000/api/v1/cars
# The curl above returns an empty JSON array "[]" if your database is empty.
# open a web browser and go to
# - http://localhost:8000/public/index.html to see the "front page"
# - http://localhost:8000/public/addnewcar.html to add a new car through a form
```


## Building Carmanager API

You need the following:

- `rustup` with the nightly toolchain
    - Install `rustup` from [the internet](https://rustup.rs/) or via your package manager
    - Activate the nightly toolchain: `rustup toolchain install nightly`
      This is needed for Rocket.
- `libpq` for the automatic PostgreSQL migrations
    - `libpq` should be included in `postgresql-libs` packages from your Linux package manager.
- Optional: `docker` or `podman` and `docker-compose` or `podman-compose`
    - For building and running Carmanager API as a container.
    - Tip: Using the compose method for running means that you may use a containerized PostgreSQL.
- Optional: `make`
    - The project includes a Makefile which helps with building either just the executable,
      or the container (requires `docker` or `podman`), or both.
- Optional: `grcov`
    - The project uses `grcov` to calculate test coverage and generate an HTML report from it.
      The coverage calculation can be easily done with the included Makefile.
    - [Link to grcov's site](https://lib.rs/crates/grcov).
    - Install with `cargo install grcov` and add it to your `PATH`.

### The executable

```bash
# Build the Debug build
cargo +nightly build

# or to build the Release build
cargo +nightly build --release
```

### Docker or Podman

```bash
# Build the Docker container
sudo docker build -t carmanager_api .

# Build the Podman container
sudo podman build -t carmanager_api .

```

### Build with make

```bash
# Building the debug and release executables
make cargo-debug
make cargo-release

# Building the debug or release containers (defaults to using podman)
make container-debug
make container-release
# Alternatively, docker-debug and podman-debug can be used to explicitly use either tool

# Building the everything (either debug or release)
make debug
make release

# Clean up the build files (note: this requires a rebuild of all Cargo crates)
make clean
```

## Running Carmanager API

### Prerequisites

Before running Carmanager API with the compose YAMLs, be sure to create a `.env` file.
`docker-compose` and `podman-compose` will use the environment variables from this file to decide
e.g. the database name and its credentials.
Also when running `carmanager_api` without a container, it will try to read the `.env` file and
look for the `DATABASE_URL` variable. It is not mandatory to have an `.env` file, but it may be
helpful.

### Without container

```bash
# Assuming that there is a PostgreSQL database running at localhost:5432

# Either specify the DATABASE_URL on the command line:
DATABASE_URL="postgres://${POSTGRES_USER}:${POSTGRES_PW}@localhost:5432/${POSTGRES_DB}" cargo +nightly run

# Or write the DATABASE_URL to a .env file to reuse it for subsequent executions
echo "DATABASE_URL=postgres://${POSTGRES_USER}:${POSTGRES_PW}@localhost:5432/${POSTGRES_DB}" | tee -a .env
# The command above is only needed once. Check the value with 'cat .env' if needed.
# Now you can just use 'cargo run' each time. Remember the +nightly toolchain.
cargo +nightly run
```

### Docker or Podman

- Remember to set the database variables in `.env` before running with compose.

```bash
# Start the containers with Docker
sudo docker-compose -f docker-compose.yml up -d

# Start the containers with Podman
sudo podman-compose -f podman-compose.yml up -d

# To build and run with compose, you can also use make
# Debug:
make run-docker
make run-podman
# Release:
make run-docker-release
make run-podman-release
```

## Running the tests

### Test database required

Some tests, e.g. the Rocket integration tests require a database to be running.
The database needs to always be empty when the tests start running. Otherwise, the tests will fail.
This is easily achieved by running the postgresql database in a container, without mounting the data
to a persistent location (i.e. your file system). This means that the data is destroyed when the
container is destroyed.

If you have Docker, docker-compose, and Make installed, you can make use of
the Makefile's `test` target. It will automatically generate some environment variables and start
a test database before launching the Rust tests. It will also pass the needed `DATABASE_URL` variable
with the PostgreSQL connection string to the `cargo test` command.

```bash
# The tests are executed with 'make'
# Note that they require docker, docker-compose, and make.
make test
```

Podman and podman-compose are not currently supported in the Makefile, since podman-compose didn't
support using a custom `--env-file` when composing. This might be an artificial blocker, and could
possibly be implemented in some other way. E.g. by running a container with `docker run` or `podman run`
and specifying all the needed parameters and variables on the command line.

### Unit tests

There are some unit tests as well, which don't require any extra containers running.
These can be run with cargo. With the commands below, the Rocket tests which rely on the database
will fail, but you can get around this by specifying a list of tests to run.

```bash
# Unit tests can be run with cargo.
cargo +nightly test
# Add '-- --no-capture' if you want to see the stdout output from the code which is executed.
cargo +nightly test -- --no-capture
# Specify a list of tests to run, if you want to only run some tests. E.g. tests including "car" in their name.
cargo +nightly test car
```

### Test coverage

Test coverage calculation requires the `grcov` tool to be installed.
To calculate test coverage, the code needs to be recompiled with certain build options,
and the tests need to be executed.
There is a target for this in the Makefile, which can be used to do this automatically.
Note that this requires the test database container setup, which is described above.

```bash
# Recompile and run the tests with the Makefile's help
make coverage
# Open the HTML report to see the coverage.
```

The coverage calculation is described more generally [on the grcov website](https://lib.rs/crates/grcov).
